public class Refactor03 {
	//Definimos las variables como globales y la opcion la cual va elegir segun se cumpla la condición
	public static int colPeonNegra=1,filPeonNegra=3,colPeonBlanca=1,filPeonBlanca=3,sigmovColPeonNegra,sigmovFilPeonNegra;
	public static int option = 1;
	
	public static void main(String[] args) {
		compruebaPosicion();
		añadePosicion();
	}
	//Comprueba si se cumplen las condiciones y cambiamos de opcion
	public static void compruebaPosicion() {
		if((colPeonNegra++==colPeonBlanca)&&(filPeonNegra++==filPeonBlanca)){
			option = 1;
		}else if((colPeonNegra++==colPeonBlanca)&&(filPeonNegra--==filPeonBlanca)){
			option = 2;
		}else if((colPeonNegra++!=colPeonBlanca)){
			option = 3;
		}
	}
	//Mueve el peon segun la opcion que hemos elegido
	public static void añadePosicion() {
		switch (option) {
			case 1: sigmovColPeonNegra=colPeonNegra++;
				sigmovColPeonNegra=filPeonNegra++;
				break;
			case 2: sigmovColPeonNegra=colPeonNegra++;
				sigmovColPeonNegra=filPeonNegra--;
				break;
			case 3: sigmovColPeonNegra=colPeonNegra++;
				break;
		}

	}

}
